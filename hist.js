var guesses = [
    [3.0, 44],
    [5.0, 43],
    [4.0, 31],
    [8.0, 28],
    [10.0, 28],
    [20.0, 27],
    [6.0, 25],
    [15.0, 21],
    [7.0, 17],
    [2.0, 12],
    [16.0, 12],
    [9.0, 11],
    [30.0, 10],
    [12.0, 10],
    [25.0, 8],
    [13.0, 6],
    [17.0, 5],
    [40.0, 5],
    [28.0, 4],
    [0.0, 4],
    [1.0, 4],
    [11.0, 4],
    [18.0, 3],
    [21.0, 3],
    [14.0, 3],
    [60.0, 3],
    [50.0, 3],
    [80.0, 2],
    [32.0, 2],
    [7.5, 2],
    [24.0, 2],
    //[150.0, 2],
    [55.0, 2],
    [23.0, 2],
    [36.0, 2],
    //[300.0, 2],
    //[120.0, 2],
    [22.0, 2],
    [43.0, 1],
    [3.3, 1],
    [3.33, 1],
    [3.5, 1],
    [2.5, 1],
    [90.0, 1],
    [75.0, 1],
    [19.0, 1],
    [33.0, 1],
    [4.5, 1],
    [42.0, 1],
    [2.7, 1],
    [100.0, 1],
    [57.0, 1],
    [2.8, 1]
];
    

// Now create the chart
Highcharts.chart('hist', {
    chart: {
        type: 'column',
    },

    plotOptions: {
        series: {
            pointWidth: 3
        }
    },
    credits: {
        enabled: false
    },
    title: {
        text: 'Kolik fotbalových hřišť se vejde na Václavské náměstí?'
    },
    subtitle: {
        text: 'anketa na sociálních sítích'
    },
    annotations: [{
        labelOptions: {
            shape: 'connector',
            align: 'right',
            justify: false,
            crop: true,
            style: {
                fontSize: '0.8em',
                textOutline: '1px white'
            }
        },
        labels: [{
            point: {
                xAxis: 0,
                yAxis: 0,
                x: 6,
                y: 25
            },
            text: 'správně'
        }, {
            point: {
                xAxis: 0,
                yAxis: 0,
                x: 100,
                y: 1
            },
            text: 'wtf?'
        }]
    }],

    xAxis: {
        labels: {
            format: '{value}'
        },
        minRange: 5,
        title: {
            text: 'Počet hřišť'
        }
    },
    yAxis: {
        min: 0,
        startOnTick: true,
        endOnTick: false,
        title: {
            text: 'Počet tipů'
        },
        labels: {
            format: '{value}'
        }
    },
    tooltip: {
        headerFormat: '<b>počet hřišť: {point.x:.0f}</b><br>',
        pointFormat: 'počet tipů: {point.y}',
        shared: true
    },

    legend: {
        enabled: false
    },

    series: [{
        data: guesses,
        lineColor: '#cb181d',
        color: '#cb181d',
        name: 'guesses',
        marker: {
            enabled: false
        },
        threshold: null
    }]

});